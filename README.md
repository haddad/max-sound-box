A collection of [Max](https://forum.ircam.fr/projects/detail/max-8/) externals with help files and examples for real-time interactions including tools for real-time analysis, transformations and synthesis of sound. The package currently includes the following modules:

## Analysis ##

- **Iana**: analysis and extraction of significant perceptive components of sound in real-time.
- **Yin**: analysis and estimation of the fundamental frequency of real-time audio. Including polyphonic harmonizer for the transposition of monophonic sounds that maintain timbre via the psych~ and psychoirtrist~ modules.
- **Ircam Descriptors**: the ircamdescriptor~ Max external calculates over 40 audio features in real-time or in batch from a buffer~, e.g. pitch, chroma, loudness, spectral moments, rolloff and crest, harmonics, perceptual features, MFCC…

## Synthesis ##

- **Pags**: modules for solo voice synthesis (sdif.pagsolo~) and virtual choirs (sdif.pagsemble~) that were first created for the [opera K…](http://www.philippemanoury.com/?p=305) by [Philippe Manoury](https://www.philippemanoury.com).
- **Sogs**: granular synthesis (looping very short sound samples) via the sogs~ and rogs~ modules.
- **Chant Objects**: modules for the synthesis of voices and instruments based on formant modeling (FOF).
- **Chromax**: spectral template generated via harmonic and formant templates, useful for dynamic control of spectral processing. Comes with two examples for spectral filtering and spectral delay.

Max Sound Box technologies are completed by [Forum Max Apps](https://forum.ircam.fr/projects/detail/forum-max-apps/) providing tutorials and ready-to-use modules, accessible directly via the download page.
